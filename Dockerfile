FROM python:3.7-alpine
LABEL maintainer="London App Developer Ltd"
ENV PYTHONUNBUFFERED 1
ENV PATH="/scripts:${PATH}"

RUN pip install --upgrade pip

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev
RUN pip install -r /requirements.txt
RUN apk del .tmp-build-deps

RUN mkdir /app
WORKDIR /app
RUN chmod +x /app
COPY . .
COPY ./app/manage.py /app
COPY ./scripts/ /scripts/
RUN chmod +x /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web
USER user
WORKDIR /app
COPY ./app/manage.py /app/
RUN cd /app
RUN ls -a
CMD ["entrypoint.sh"]
#CMD python manage.py collectstatic --noinput && python manage.py wait_for_db && python manage.py migrate && uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi && python manage.py runserver 0.0.0.0:8000
#CMD ["%%CMD%%"] 

 