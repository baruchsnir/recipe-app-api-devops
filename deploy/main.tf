terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-bs"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}
provider "aws" {
  shared_credentials_file = "~/.aws/credentials"
  region                  = "us-east-1"
  # access_key = "AKIAIT3L5CERWBQUYW6A"
  # secret_key = "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"
}
# provider "aws" {
#   region  = "us-east-1"
#   version = "~> 2.61.0"

#   access_key = "AKIAIT3L5CERWBQUYW6A"
#   secret_key = "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"
# }

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}




