variable "prefix" {
  default = "raad"
}
variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "baruchsnir@hotmail.com"
}
