data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

#https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19173970#search
#go to https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#LaunchInstanceWizard: and select the first 
#Amazon Linux 2 AMI (HVM), SSD Volume Type - ami-04d29b6f966df1537 (64-bit x86) / ami-03156384f702d4eaf (64-bit Arm)
#take the ami-04d29b6f966df1537 and search in next link and copy it to and get the 	amzn2-ami-hvm-2.0.20201126.0-x86_64-gp2
# and replace 20201126.0 with *
#https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Images:visibility=public-images;search=ami-04d29b6f966df1537;sort=desc:nam
resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  # tags = {
  #   Name = "${local.prefix}-bastion"
  # }
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}
output "tags" {
  value = aws_instance.bastion.tags

}

# resource "aws_iam_role" "bastion" {
#   name               = "${local.prefix}-bastion"
#   assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

#   tags = local.common_tags
# }

# resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
#   role       = aws_iam_role.bastion.name
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
# }

# resource "aws_iam_instance_profile" "bastion" {
#   name = "${local.prefix}-bastion-instance-profile"
#   role = aws_iam_role.bastion.name
# }

# resource "aws_instance" "bastion" {
#   ami                  = data.aws_ami.amazon_linux.id
#   instance_type        = "t2.micro"
#   user_data            = file("./templates/bastion/user-data.sh")
#   iam_instance_profile = aws_iam_instance_profile.bastion.name
#   key_name             = var.bastion_key_name
#   subnet_id            = aws_subnet.public_a.id

#   vpc_security_group_ids = [
#     aws_security_group.bastion.id
#   ]

#   tags = merge(
#     local.common_tags,
#     map("Name", "${local.prefix}-bastion")
#   )
# }

# resource "aws_security_group" "bastion" {
#   description = "Control bastion inbound and outbound access"
#   name        = "${local.prefix}-bastion"
#   vpc_id      = aws_vpc.main.id

#   ingress {
#     protocol    = "tcp"
#     from_port   = 22
#     to_port     = 22
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     protocol    = "tcp"
#     from_port   = 443
#     to_port     = 443
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     protocol    = "tcp"
#     from_port   = 80
#     to_port     = 80
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port = 5432
#     to_port   = 5432
#     protocol  = "tcp"
#     cidr_blocks = [
#       aws_subnet.private_a.cidr_block,
#       aws_subnet.private_b.cidr_block,
#     ]
#   }

#   tags = local.common_tags
# }
